package com.example.app;

import com.example.dep.Dep;

public class App 
{
    public static void main( String[] args )
    {
        Dep.hello( "GitLab" );
        int a=10;
        int b=10;
        int c;
        c=a+b;
        System.out.println(c);
    }
}
